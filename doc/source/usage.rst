========
Usage
========

At the moment, this package provides three commands.

``ciwatch-server``. Launch a development server.

``ciwatch-stream-events``. Stream events from Gerrit and append valid
events to ``third-party-ci.log``.

``ciwatch-populate-database``. Add all entries from
``third-party-ci.log`` to the database.
