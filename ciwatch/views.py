# Copyright (c) 2015 Tintri. All rights reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from flask import render_template
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.exceptions import abort

from ciwatch.api import get_context
from ciwatch.cache import cached
from ciwatch import db
from ciwatch.server import app


@app.route("/")
@app.route("/index")
@app.route("/home")
def home():
    try:
        return render_template(
            'index.html.jinja', projects=db.get_projects())
    finally:
        db.Session.remove()


@app.route("/project")
@cached
def project():
    try:
        return render_template("project.html.jinja", **get_context())
    except NoResultFound:
        abort(404)
    finally:
        db.Session.remove()
